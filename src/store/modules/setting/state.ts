export interface SettingState {
    showTitle: boolean
}

const state: SettingState = {
    showTitle: true
}
export default state
